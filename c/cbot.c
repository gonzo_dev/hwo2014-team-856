//./run senna.helloworldopen.com 8091 

#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *joinRace_msg(char *bot_name, char *bot_key, char *trackName, char *password, int car_count);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);
static cJSON *turn_left_msg();
static cJSON *turn_right_msg();
static void setRaceData(cJSON *jsonData);
static float getSecLength( int laneId, int sectionId);


static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);
static float getThrottle();
static void SetCurrentPosition( cJSON *dataJson);

#define MAX_SECTIONS (100)
#define MAX_LANES (10)

typedef struct lane_t
{
	float dist;
	int index;
} lane;

typedef struct section_t
{
	float length;
	float radians;
	int switchlane; //0 = false,1+=true
	float radius;
	float angle; //Degrees
} section;

//Let the global madness begins!!
char g_mycarName[256];
int g_numSections = 0;
int g_numLanes = 0;
section g_sectionList[MAX_SECTIONS];
lane g_laneList[MAX_LANES];
int g_cur_id = 0;
float g_pos_in_sec = 0.0f;
float g_pos = 0.0f;
float g_current_speed = 0.0f;

//Returns the pieceId of our car
static void SetCurrentPosition( cJSON *dataJson)
{
	//		puts ("START GetCurrentPosition");
	int i,mycar,numCars;
	cJSON *subitem, *idJson, *nameJson;
	cJSON *piecePosJson, *pieceIdJson, *pieceDistanceJson;
	cJSON *laneJson, *startLaneJson, *endLaneJson;
	
	numCars = cJSON_GetArraySize(dataJson);
	mycar = numCars+1;
	
	for (i = 0 ; i < numCars ; ++i)
	{
		subitem = cJSON_GetArrayItem(dataJson, i);
		idJson = cJSON_GetObjectItem(subitem, "id");
		nameJson = cJSON_GetObjectItem(idJson, "name");
		
		if (!strcmp (nameJson->valuestring, g_mycarName))
		{
			mycar = i;
		}
	}
	if (mycar > numCars)
	{
		puts("ERROR: not able to get car position");
		puts (g_mycarName);
		g_cur_id = 0;
		g_pos_in_sec = 0.0f;
	}
	
	//Get our Car item
	subitem = cJSON_GetArrayItem(dataJson, mycar);
	piecePosJson = cJSON_GetObjectItem(subitem, "piecePosition");
	pieceIdJson = cJSON_GetObjectItem(piecePosJson, "pieceIndex");
	pieceDistanceJson = cJSON_GetObjectItem(piecePosJson, "inPieceDistance");
	laneJson = cJSON_GetObjectItem(piecePosJson, "lane");
	startLaneJson = cJSON_GetObjectItem(laneJson, "startLaneIndex");
	endLaneJson = cJSON_GetObjectItem(laneJson, "endLaneIndex");
	
	float prev_pos = g_pos + g_pos_in_sec;
	int new_id = pieceIdJson->valueint;
	float new_pos_in_sec = pieceDistanceJson->valuedouble;
	float new_pos;
	int cur_lane = startLaneJson->valueint; //For testing let's take the starting.
	//NOTE:TODO: CALCULATE Current velocity and proceed 
	if (new_id == g_cur_id)
	{
		new_pos = g_pos + new_pos_in_sec;
	}
	else
	{
		int prev_id = new_id-1;
		if (prev_id < 0) prev_id = g_numSections-1;
		g_pos +=  getSecLength (cur_lane, prev_id);//g_sectionList[prev_id].real_length;
		new_pos = g_pos + new_pos_in_sec;
	}
	//float new_pos = g_pos + 
//puts ("END GetCurrentPosition");
	g_cur_id = new_id;
	g_pos_in_sec = new_pos_in_sec;
	g_current_speed = new_pos - prev_pos;
	float cur_angle = g_sectionList[g_cur_id].angle;
	float radius = g_sectionList[g_cur_id].radius;
	
	printf ("[%d] Speed:%02.2f-Pos:%02.2f [%02.2f-%02.2f]-angle=%02.2f-radius:%02.2f-L:%2.2f\n",g_cur_id,g_current_speed,new_pos,g_pos,new_pos_in_sec,cur_angle,radius,g_laneList[cur_lane].dist);
}

static float getThrottle()
{
	float throt = 0.0f;
	int prevId = g_cur_id - 1;
	int nextId = g_cur_id + 1;
	if (nextId >= g_numSections) nextId =0;
	if (prevId < 0) prevId = g_numSections-1;
	if (g_cur_id >= g_numSections) 
	{
		puts ("Error: wrong ID");
		throt = 0.0f;
	}
	else
	{
		if (g_sectionList[g_cur_id].length > 0.0f && g_sectionList[nextId].length > 0.0f)
		{
			//We are in a straight line and next is straight line.
			throt = 1.0f;
		}
		else
		{
			//Depending of the leftovers of the distance on the straight line.
			float diffDis = (g_sectionList[g_cur_id].length - g_pos_in_sec);
			if (g_sectionList[prevId].length >0)
			{
			//We apply this just when we come from 2 straights lines into a curve
				if ( diffDis >= 70.0f)
					throt = 0.7f;
				else if (diffDis >= 40.0f)
					throt = 0.4f;
				else if (diffDis >= 30.0f)
					throt = 0.1f;
				else
					throt = 0.1f; //close to curve and in the curve.
			}
			else 
			{
				throt = 0.7f;
			}
			
			//We are in a curve
			if (g_sectionList[g_cur_id].radius > 0.0f)
			{
				//We have a radius
				//Let's do a simple proportional calculation for now. 
				//Higher the number, the more we have to brake.
				float diff = g_sectionList[g_cur_id].radius * g_sectionList[g_cur_id].angle;
				float value = abs(diff) /10000.0f * 1.0f;
				throt = 1 - value;
				
				//We are in a curve and next section is a straigth line
				if (g_sectionList[nextId].length > 0.0f)
				{
					//if after is a straight line and not another curve
					//if ( g_pos_in_sec <= 10.0f)
					//	throt -= 0.1f;
					//else if (g_pos_in_sec <= 20.0f)
					//	throt -= 0.05f;
					//else if (g_pos_in_sec <= 30.0f)
					//	throt -= 0.05f;
					//else
					if (g_pos_in_sec >= 50.0f && g_pos_in_sec < 60.0f)
						throt += 0.3f;
					else if (g_pos_in_sec >= 60.0f)
						throt += 0.5f;
				}
				else
				{
				//We are in a curve and next is a curve
					if ( g_pos_in_sec <= 10.0f)
						throt -= 0.5f;
					else if (g_pos_in_sec <= 20.0f)
						throt -= 0.4f;
					else if (g_pos_in_sec <= 30.0f)
						throt -= 0.3f;
					else if (g_pos_in_sec <= 40.0f)
						throt -= 0.2f;
					else if (g_pos_in_sec <= 50.0f)
						throt -= 0.1f;
				}
				
				if (throt <= 0.0f) throt = 0.05f;
				else if (throt > 1.0f) throt = 1.0f;
				
				//printf ("[%d]-PosInSec:%f,diff:%f,value:%f,throttle: %f \n",g_cur_id,g_pos_in_sec,diff,value, throt);
			}
			//else
				//printf ("[%d]-diffDis:%f,throttle: %f \n",g_cur_id,diffDis, throt);
		}
	}
	return throt;
}

 static void setRaceData(cJSON *jsonData)
 {

	cJSON *dataJson, *raceJson, *trackJson, *idJson, *nameJson, *piecesJson, *lanesJson;
	char *text1, *text2;
	int i;
	
	dataJson = cJSON_GetObjectItem(jsonData, "data");
	if (dataJson == NULL)
		puts("missing msgType field dataJson");
	raceJson = cJSON_GetObjectItem(dataJson, "race");
	trackJson = cJSON_GetObjectItem(raceJson, "track");
	idJson = cJSON_GetObjectItem(trackJson, "id");
	nameJson = cJSON_GetObjectItem(trackJson, "name");
	text1 = idJson->valuestring;
	text2 = nameJson->valuestring;
	puts( text1 );
	puts( text2 );
	
	//Get Lanes of track
	lanesJson = cJSON_GetObjectItem(trackJson, "lanes");
	g_numLanes = cJSON_GetArraySize(lanesJson);
	for (i = 0 ; i < g_numLanes ; ++i)
	{
		cJSON *distJson, *idxJson;
		cJSON *subitem;
		subitem = cJSON_GetArrayItem(lanesJson, i);
		distJson = cJSON_GetObjectItem(subitem, "distanceFromCenter");
		idxJson = cJSON_GetObjectItem(subitem, "index");
		int idx = idxJson->valueint;
		g_laneList[idx].dist = distJson->valuedouble;
		g_laneList[idx].index = idx; //? TODO: test this bullsh*t
		printf("LANE[%d]=idx=%d - dist:%2.2f\n", i, idx,g_laneList[idx].dist);
	}
	
	//Get Pieces of track (Sections)
	piecesJson = cJSON_GetObjectItem(trackJson, "pieces");
	g_numSections = cJSON_GetArraySize(piecesJson);
	for (i = 0 ; i < g_numSections ; ++i)
	{
		cJSON *lengthJson, *radiusJson, *angleJson, *switchedJson;
		cJSON *subitem;
		subitem = cJSON_GetArrayItem(piecesJson, i);
		
		lengthJson = cJSON_GetObjectItem(subitem, "length");
		radiusJson = cJSON_GetObjectItem(subitem, "radius");
		angleJson = cJSON_GetObjectItem(subitem, "angle"); 
		switchedJson = cJSON_GetObjectItem(subitem, "switch"); 
		
		if (lengthJson)   g_sectionList[i].length = lengthJson->valuedouble; else g_sectionList[i].length = 0.0f;
		if (radiusJson)   g_sectionList[i].radius = radiusJson->valuedouble; else g_sectionList[i].radius = 0.0f;
		if (angleJson)    g_sectionList[i].angle = angleJson->valuedouble; else g_sectionList[i].angle = 0.0f;
		//g_sectionList[i].switchlane = switchedJson && !strcmp(switchedJson->valuestring,"true");
		
		//Calculate Radians
		if (g_sectionList[i].length >0)
		{
			g_sectionList[i].radians = 0.0f;// g_sectionList[i].length;
		}
		else
		{
			//float PI = 3.1416f;
			float PI = 3.14159265358979f;
			//double PI = M_PI;
			g_sectionList[i].radians = (2.0f * PI * abs(g_sectionList[i].angle)) / 360.0f;
		}
		//char longstring[256] = {0};
		printf("SEC[%d]=L:%f;R:%f;A:%f;S:%d;Rad:%f\n",i,g_sectionList[i].length,g_sectionList[i].radius,g_sectionList[i].angle,g_sectionList[i].switchlane,g_sectionList[i].radians);
		//puts (longstring);
	}
	
	//cJSON_Delete(nameJson);
	//cJSON_Delete(idJson);
	//cJSON_Delete(trackJson);
	//cJSON_Delete(raceJson);
	//cJSON_Delete(dataJson);
	
 }
 
 static float getSecLength( int laneId, int sectionId)
 {
	if (laneId >= g_numLanes) return 0.0f;
	if (sectionId >= g_numSections) return 0.0f;
	if (g_sectionList[sectionId].radius == 0) return g_sectionList[sectionId].length;
	
	return (abs(g_sectionList[sectionId].radius) + g_laneList[laneId].dist) * g_sectionList[sectionId].radians;
 }

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        printf("[%d] -Someone crashed\n",g_cur_id);
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}

int main(int argc, char *argv[])
{
    int sock;
	int switched;
	//float timming =0.0f;
    cJSON *json;
	int i;
	for (i =0; i< argc; ++i)
	{
		printf ("%d:%s\n",i,argv[i]);
	}
	printf ("\n");
	
    if (argc != 8)
        error("Usage: bot host port botname botkey trackname password carcount\n");

    sock = connect_to(argv[1], argv[2]);
	int carCount = atoi (argv[7]);
	json = join_msg(argv[3], argv[4]);
    //json = joinRace_msg(argv[3], argv[4], argv[5], argv[6], carCount);
    write_msg(sock, json);
    cJSON_Delete(json);
	switched = 0;
	
    while ((json = read_msg(sock)) != NULL)
	{
        cJSON *msg, *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;
        if (!strcmp("carPositions", msg_type_name)) 
		{
			//puts ("START CAR POS");
			cJSON *dataJson = cJSON_GetObjectItem(json, "data");

			SetCurrentPosition(dataJson);
			float throt_now = getThrottle();
			msg = throttle_msg(throt_now);
			/*
			if (timming >= 1.0f)
			{
				msg = throttle_msg(0.79f);
				//timming=0.0f;
				//puts("--->Speed UP");
			}
			else
			{
				msg = throttle_msg(0.655999999999);//min not crashing 0.6559999, 0.656 -it crashes.
				//puts("--->Speed ====");
			}
			timming+=0.01f;
			*/
			//puts ("END CAR POS");
        } else
		{
			if (!strcmp("gameInit", msg_type_name)) 
			{
				puts ("START INIT");
				setRaceData(json);
				puts ("END INIT");
			}
			if (!strcmp("yourCar", msg_type_name)) 
			{
				puts ("START MY CAR");
				cJSON *dataJson, *nameJson;
				dataJson = cJSON_GetObjectItem(json, "data");
				nameJson = cJSON_GetObjectItem(dataJson, "name");
				strcpy (g_mycarName, nameJson->valuestring);
				puts (g_mycarName);
				puts ("END MY CAR");
			}
            log_message(msg_type_name, json);
			//if (switched ==0)
			{
			//	msg = turn_right_msg();
			//	switched = 1;
			}
			//else
			{
				msg = ping_msg();
			}
        }

        write_msg(sock, msg);

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }

    return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

//Advanced

static cJSON *joinRace_msg(char *bot_name, char *bot_key, char *trackName, char *password, int car_count)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *botId = cJSON_CreateObject();
    cJSON_AddStringToObject(botId, "name", bot_name);
    cJSON_AddStringToObject(botId, "key", bot_key);
    cJSON_AddItemToObject(data, "botId", botId);
    cJSON_AddStringToObject(data, "trackName", trackName);
    cJSON_AddStringToObject(data, "password", password);
    cJSON_AddNumberToObject(data, "carCount", car_count);
	
    cJSON *json = make_msg("joinRace", data);
	
	printf ("JOINRACE:%s",cJSON_Print(json));
	
    return json;
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *turn_left_msg()
{
    return make_msg("switchLane", cJSON_CreateString("Left"));
}

static cJSON *turn_right_msg()
{
    return make_msg("switchLane", cJSON_CreateString("Right"));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}
